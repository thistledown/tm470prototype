/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author davidpretty
 */
public class ClassifiedMessageTest
{
    private Session session;
    
    
    /**
     * Not used except by test framework
     */
    public ClassifiedMessageTest()
    {
    }
    
    /**
     * Not used
     */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    
    /**
     * Creates a session instance so the tests can be run
     */
    @Before
    public void setUp()
    {
        Properties props = Utilities.getProperties("storeProperties");
        session = Session.getInstance(props);
    }
    
    
    /**
     * Not used-should I be getting rid of Session objects? Look up
     */
    @After
    public void tearDown()
    {
    }

    /**
     * tests constructing a simple ClassifiedMessage without tags
     * works correctly, that the  text and tags are correct. Does not 
     * test message id as this cannot be set without sub-classing MimeMessage
     * and overriding the inherited method and testing a Message object equals
     * another is problematic as Message does not over-ride the equals() method
     * inherited from Object so are only comparing hashcodes.
     * 
     */
    @Test
    public void testConstructorNoTags()
    {
        String testText = "Some text mentioning Bloombury Court and Willow Court";
        Set<String> testTags = new HashSet();
        testTags.add("BCX");
        testTags.add("WCX");
        String testID = "test.test.com";
        try
        {
            Message testMsg = TestMessageUtil.makePlainTextMsg(session, testText);
            ClassifiedMessage classifiedTestMsg = new ClassifiedMessage(testMsg,
                                                     testText, testID, testTags);
            assertEquals("The ClassifiedMessage has the same text as the " +
                            "original message",
                            testText, classifiedTestMsg.getMessageText());
            assertEquals("The ClassifiedMessage has the specified tags",
                            testTags, classifiedTestMsg.getTags());    
        }
        catch (MessagingException mex)
        {
            fail(mex.getMessage());
        }
        
        
    }
    
}
