/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author davidpretty
 * Class for experimenting with jsoup
 */
public class JsoupSandpit
{
    /**
     * Test method for whatever bit of jsoup I was experimenting with most
     * recently
     * @param args 
     */
    public static void main(String[] args)
    {
        String enriched = "<bold>Now</bold> is the time for <italic>all</italic>" +
         "good men     <smaller>(and <<women>)</smaller> to" +
         "<ignoreme>come</ignoreme>";
        Document doc = Jsoup.parse(enriched);
        System.out.println(doc.text());
        
        
    }
}
