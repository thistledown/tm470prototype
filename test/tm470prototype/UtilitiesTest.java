/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.Properties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class to test methods of Utilities class
 * @author davidpretty
 */
public class UtilitiesTest
{
    /**
     * Only used by test framework
     */
    public UtilitiesTest()
    {
    }
    
    
    /**
     * Not used
     */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    /**
     * Not used
     */
    @Before
    public void setUp()
    {
    }
    
    /**
     * Not used
     */
    @After
    public void tearDown()
    {
    }

  /**
   * Tests that method returns a property file with the expected values
   */
  @Test 
  public void testGetProperities()
  {
      Properties storeProps = Utilities.getProperties("storeProperties");
      if(storeProps != null)
      {
          assertTrue("getProperties shoud return a Properies object",
              storeProps instanceof Properties);
          assertEquals("getProperty(\"host\") should return  \"outlook.office365.com\"",
                  "outlook.office365.com", storeProps.getProperty("host"));
      }
      else
      {
          fail("return value is null");
      }
      assertTrue("getProperties shoud return a Properies object",
              storeProps instanceof Properties);
  }
    
}
