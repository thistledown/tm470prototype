/*
 
 */

package tm470prototype;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 Test file for the Bayesian Tagger
 * @author davidpretty
 */
public class BayesianTaggerTest
{
    /**
     * Not used except by test framework
     */
    public BayesianTaggerTest()
    {
    }
    
    /**
     * Not used at present
    */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used at present
    */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    /** 
     * Not used at presnt
     */
    @Before
    public void setUp()
    {
    }
    
    /**
     * Not used at present
     */
    @After
    public void tearDown()
    {
    }

    /**
     * Tests at the the most basic level, that if the tagger is  trained on one message 
     * with associated tags, that it will return the correct set of tags
     * if passed the same message text again
     */
    @Test public void testGetCorrectTagsIfTrainedOn1Message()
    {
        Set<String> testTags = new HashSet(Arrays.asList(
                                new String[]{"DCHU01","RCRC001D006"}));
        Properties props = Utilities.getProperties("storeProperties");
        String folderName = props.getProperty("folder");
        
        try
        {
            MessageStore msgStore = new MessageStore(props);
            msgStore.setFolder(folderName);
            
            
            String testID = Utilities.getFirstMessageIDFromCSV("test/" +
                    "tm470prototype/resources/testSecond100.csv");
            
            Message testMsg = msgStore.getMessageByID(testID);
            Tagger bayesTagger = new BayesianTagger();
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(testMsg);
            
            ClassifiedMessage classifiedMessage = new ClassifiedMessage(
                testMsg, msgText, testID, testTags);
            Set<ClassifiedMessage> trainingSet = new HashSet();
            trainingSet.add(classifiedMessage);
            bayesTagger.train(trainingSet);
            
            assertEquals("The tagger returns the original tags for the message",
                    testTags, bayesTagger.getTags(msgText));
        }
        catch (MessagingException mex)
        {
            fail(mex.getMessage());
        }
    }
    
}
