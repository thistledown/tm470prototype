/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Class for testing MultipartAlternativeMimeChecker, which is 
 * a utility class for listing the MimeTypes in a message
 * @author davidpretty
 */
public class MultipartAlternativeMimeTypeCheckerTest
{
    private Store store;
    private String host, user, password;
    private Properties props;
    private Session session;
    
    
    /**
     * Constructor, used by test framework?
     */
    public MultipartAlternativeMimeTypeCheckerTest()
    {
    }
    
    /**
     * Not used at present
     */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used at present
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    /**
     * Sets up objects for text fixture. Hard codes in Soho Housing information
     * so desperately needs refactoring.
     */
    @Before
    public void setUp()
    {
        store = null;
        
        host = "outlook.office365.com";
        user = "david2@sohoha.org.uk";
        password = "Password!";
        props = new Properties();
        props.setProperty("mail.imap.ssl.enable","true");
        props.setProperty("mail.imap.port", "993");
        session = Session.getInstance(props);
        try
        {
             store = session.getStore("imap"); // this might need to be imaps instead
        }
        catch(NoSuchProviderException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * Closes message store. Not sure if need to do this.
     */
    @After
    public void tearDown()
    {
        try
        {
            store.close();
        }
        catch(MessagingException mex)
        {
            mex.printStackTrace();
        }
    }

    /**
     * Tests that MultipartAlternativeMimeTypeChecker can correctly identify 
     * the MimeTypes in a 2 part
     * MimeMessage, with 1 part plaintext and the other html
    @Test
    public void testIdentifyMimeTypesSimple2PartAlterative()
    {
         Message msg = null;
         try
         {
             msg = TestMessageUtil.make2PartAlternative(session, "plain text",
                     "<html>Some html</html>");
             
             MultipartAlternativeMimeTypeChecker multiAlt = 
                     new MultipartAlternativeMimeTypeChecker();
             try
             {
                 multiAlt.updateAltSet(msg);
             }
             catch(IOException io)
             {
                 fail("threW IO Exception " + io);
             }
             assertEquals("altSet should have 1 member", 1, multiAlt.getNoTypes());
             assertTrue("altSet should contain text/html",
                            multiAlt.containsSubType("text/html"));
             assertTrue("altSet shold contain text/plain",
                            multiAlt.containsSubType("text/plain"));
         }
         catch(MessagingException ex)
         {
             fail("Threw Messaging exception" + ex);
         }
    } 
    
    
    /**
     *  Tests that MultipartAlternativeMimeTypeChecker can correctly identify 
     * the MimeTypes in a single part message.
     */
    @Test
    public void testIdentifyMimeTypesNoAlternativePart()
    {
        Message msg = null;
         try
         {
             msg = TestMessageUtil.makePlainTextMsg(session,
                     "plain text");
         }
         catch(MessagingException mex)
         {
             fail("Creating test message threw Messaging Exception");
         }
         MultipartAlternativeMimeTypeChecker multiAlt = 
                     new MultipartAlternativeMimeTypeChecker();
         try
         {
             multiAlt.updateAltSet(msg);
         }
         catch(IOException io)
         {
             fail("threW IO Exception " + io);
         }
         catch(MessagingException me)
         {
             fail("threw Messaging Exception " + me);
         }
         assertEquals("multAlt.getNoTypes == 0", multiAlt.getNoTypes(), 0);
    }
        
    
    
    /**
     * Tests that MultipartAlternativeMimeTypeChecker can correctly identify 
     * the MimeTypes in a message with a RFC attachment.
     */
    @Test
    public void testIdentifyMimeTypesRFC822()
    {
        Message msg = null;
        try
         {
             msg = TestMessageUtil.makeMsgWithRFC822Attachment(session,
                     "plain text", "<html>html text</html>");
             
             MultipartAlternativeMimeTypeChecker multiAlt = 
                     new MultipartAlternativeMimeTypeChecker();
             try
             {
                 multiAlt.updateAltSet(msg);
             }
             catch(IOException io)
             {
                 fail("threW IO Exception " + io);
             }
            // multiAlt.outputAltSet();
             assertEquals("altSet should have 1 member",multiAlt.getNoTypes(),1);
             assertTrue("altSet should contain text/html",
                            multiAlt.containsSubType("text/html"));
             assertTrue("altSet shold contain text/plain",
                            multiAlt.containsSubType("text/plain"));
        }
        catch(MessagingException ex)
         {
             fail("Threw Messaging exception" + ex);
         }
    }
    
    
    /**
     * Tests that MultipartAlternativeMimeTypeChecker will correctly update
     * if passed 2 messages of the same composition of MimeTpes, that is
     * there is no double counting if 2 messages with the same composition
     * of MimeTypes are encountered.
     */
    @Test
    public void testIdentifyMimeTypesAdd2SameTypeMsg()
    {
        Message msg1, msg2;
        try
        {
            msg1 = TestMessageUtil.make2PartAlternative(session,
                    "plain text", "<html> html text</html>");
            msg2 = TestMessageUtil.make2PartAlternative(session,
                    "different plain text",
                    "<html>different html text</html>");
            MultipartAlternativeMimeTypeChecker multiAlt = 
                new MultipartAlternativeMimeTypeChecker();
            multiAlt.updateAltSet(msg1);
            multiAlt.updateAltSet(msg2);
            
            Set<String> testSet = new HashSet<>();
            testSet.add("text/html");
            testSet.add("text/plain");
            
            assertEquals("altSet should have 1 member",multiAlt.getNoTypes(),1);
            assertTrue("altSet should contain 1 type made up of" +
                    "text/html & \"text/plain",
                            multiAlt.containsType(testSet));
        }
        catch(MessagingException mex)
        {
            fail("Messaging Exception thrown: " + mex);
        }
        catch(IOException io)
        {
            fail("IOException thrown: " +io);
        }
        
    }
    
    /**
     * Tests that MultipartAlternativeMimeTypeChecker can correctly identify 
     * the MimeTypes if passed 2 messages with different composition-that is 
     * that 2 different types are recorded
     */
    @Test
    public void testIdentifyMimeTypesAdd2DiffTypeMsg()
    {
        try
        {
            Message msg1 = TestMessageUtil.make2PartAlternative(session,
                    "plain text", "<html>some html</html>");
            Message msg2 = TestMessageUtil.make3PartAlternative(session,
                    "plain text", 
                    "<html>html text</html>",
                    "rich text");
            MultipartAlternativeMimeTypeChecker multiAlt =
                    new MultipartAlternativeMimeTypeChecker();
            multiAlt.updateAltSet(msg1);
            multiAlt.updateAltSet(msg2);
            
            Set<String> testSet1 = new HashSet<String>();
            Set<String> testSet2 = new HashSet<String>();
            
            testSet1.add("text/html");
            testSet1.add("text/plain");
            
            testSet2.add("text/html");
            testSet2.add("text/plain");
            testSet2.add("text/rtf");
            
            assertEquals("multiAlt should have 2 multipart/alternative types",
                            2, multiAlt.getNoTypes());
            assertTrue("multiAlt contains testSet1", 
                    multiAlt.containsType(testSet1));
            assertTrue("multiAlt contains testSet2",
                    multiAlt.containsType(testSet2)); 
        }
        catch(MessagingException mex)
        {
            fail("Threw MessagingException: " + mex);
        }
        catch(IOException io)
        {
            fail("Threw IOException: " + io);
        }
    }
    
}
