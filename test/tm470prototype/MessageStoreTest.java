/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Class for testing MessageStore
 * @author davidpretty
 */
public class MessageStoreTest
{
    private static MessageStore msgStore;
    
    /**
     * Constructor
     */
    public MessageStoreTest()
    {
    }
    
    /**
     * Creates MessageStore objects for tests
     * @throws javax.mail.MessagingException if any problems creating Message Store
     */
    @BeforeClass
    public static void setUpClass() throws MessagingException
    {
        Properties storeProps = Utilities.getProperties("storeProperties");
        msgStore = null;
        msgStore = new MessageStore(storeProps);
        msgStore.setFolder(storeProps.getProperty("folder"));
    }
    
    /**
     * Closes MessageStore object used by test cases
     * @throws javax.mail.MessagingException if any problems closing MessageStore
     */
    @AfterClass
    public static void tearDownClass() throws MessagingException
    {
        msgStore.close();
    }
    
    /**
     * not used at present
     */
    @Before
    public void setUp()
    {
    }
    
    /**
     * not used at present
     */
    @After
    public void tearDown()
    {
    }
    
    /**
     * Tests that the single-argument constructor throws an Illegal Argument
     * Exception if passed a props object whose "protocol" property is set 
     * to an unsupported mail access protocol.
     */
    @Test
    public void testThowsExceptionIfPassedInvalidProtocol()
    {
        Properties props = new Properties();
        props.setProperty("protocol", "invalid");
        try
        {
            MessageStore msgStore = new MessageStore(props);
            fail("Constructor should throw IllegalArgumentException");
        }
        catch(IllegalArgumentException ex)
        {
            // do nothing
        }   
        catch(MessagingException mex)
        {
            fail("Problem creating message store");
        }
        
    }
    
   
    
    

//    /**
//     * Test of hasNewMessages method, of class MessageStore.
//     */
//    @Test
//    public void testHasNewMessages()
//    {
//        System.out.println("hasNewMessages");
//        MessageStore instance = null;
//        boolean expResult = false;
//        boolean result = instance.hasNewMessages();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getMesssage method, of class MessageStore.
//     */
//    @Test
//    public void testGetMesssage()
//    {
//        System.out.println("getMesssage");
//        MessageStore instance = null;
//        Message expResult = null;
//        Message result = instance.getMesssage();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
    
    /**
     * Test if can retrieve a message by its Id. Tests if the the return value
     * is a Message object and that it's id matches
     */
    @Test
    public void testGetMessageByID()
    {
        String testID = "<DB5PR03MB0997DD2AB78E2E4BBD17A6A4E6F20@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        try
        {
            Message testMessage = msgStore.getMessageByID(testID);
            if(testMessage != null)
            {
                assertTrue("getMessageByID() should return Message object",
                        testMessage instanceof Message);
                MimeMessage testMimeMessage = (MimeMessage) testMessage;
                assertNotNull("Message id should not be null",
                        testMimeMessage.getMessageID());
                assertEquals("Message with desired Message Id returned",
                        testID, testMimeMessage.getMessageID());
            }
            else
            {
                fail("return value is null");
            }
        }
        catch(MessagingException mex)
        {
            fail("Messaging Exception thrown: " + mex);
        }
        
    }
    
    
    
    /**
     * Test is the return value is an set of Messages (implicitly by assigning
     * a return value - at present I do not understand how to test the type 
     * of a generic)
     * and the MessageIDs are those of the
     * desired messages
     * 
     */
    @Test   
    public void testGetMessagesbyIDs()
    {
        String id1 = "<DB5PR03MB0997DD2AB78E2E4BBD17A6A4E6F20@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        String id2 = "<HE1PR03MB1339DED66A3250869F3863AF82F20@HE1PR03MB1339." + 
                "eurprd03.prod.outlook.com>";
        String id3 = "<HE1PR03MB133985DC3479661891F650AA82F20@HE1PR03MB1339." + 
                "eurprd03.prod.outlook.com>";
        String[] IDs = {id1, id2, id3};
        Set<String> expectedIDs = new HashSet(Arrays.asList(IDs));        
        try
        {
            Set<Message> testMessages = msgStore.getMessagesByIDs(expectedIDs);
            Message testMessage1 = msgStore.getMessageByID(id1);
            Message testMessage2 = msgStore.getMessageByID(id2);
            Message testMessage3 = msgStore.getMessageByID(id3);
            if(testMessages != null)
            {
                assertEquals("getMessagesByID() should set of size 3",
                        3, testMessages.size());
                assertTrue("Message with id1 present",
                        testMessages.contains(testMessage1));
                assertTrue("Message with id2 present",
                        testMessages.contains(testMessage2));
                assertTrue("Message with id3 present",
                        testMessages.contains(testMessage3));
            }
            else
            {
                fail("return value is null");
            }
        }
        catch(MessagingException mex)
        {
            fail(mex.getMessage());
        }    
    
    }
    
    
   /**
    * Tests getMessages() method, just checks that an object of type
    * Map<String, Message> is returned, that if the Map is not empty
    * that it contains at least one entry, that the key of the 
    * first entry is a string, if present (as at present
    * the keys are message ids and not all messages have ids),
    * and the value of the entry is a message
    */
    @Test
    public void testGetMessages()
    {
        Map<String, Message> messages = null;
        try
        {
            messages = msgStore.getMessages();
        }
        catch(MessagingException mex)
        {
            fail("problem accessing MessageStore" + mex);
        }
        
        if(messages == null)
        {
            fail("null returned, expecting Map<String, Message>");
        }
        
        if(messages.size() == 0)
        {
            //Test passes as this is valid if the folder is empty.
        }
        else
        {
            Map.Entry<String, Message> entry = 
                    messages.entrySet().iterator().next();
            Message message = entry.getValue();
            assertTrue("key of random entry of Map returned by " +
                    "getMessages is a String if present: " + entry.getKey(), 
                    entry.getKey() == null ||
                            entry.getKey() instanceof String);
            assertTrue("Value of random entry of Map returned by " +
                    "getMessages is a Message",
                    entry.getValue() instanceof Message);
        }
            
            
    }
        
  /**Throwaway method to see what the actual IDs of the message store
   * 
   */      
   @Test
   public void listMessageIDsInStore()
   {
       fail("Method not written yet, dependent on MessageStore.getMessages()");
   }
    
}
