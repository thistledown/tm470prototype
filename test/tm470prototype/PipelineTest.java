/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author davidpretty
 * Using this class for end to end tests which is probably an abuse of
 * Junit but do not have time to learn a new framework.
 * 
 * 
 * 
 */
public class PipelineTest
{
    
    /**
     * Constructor,not used at present
     */
    public PipelineTest()
    {
    }
    
    
    /** 
    Not used at present
    */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    
    /**
     * Not used at present
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    /**
     * Not used at present
     */
    @Before
    public void setUp()
    {
    }
    
    /**
     * Not used at present
     */
    @After
    public void tearDown()
    {
    }


    /**
     * Runs end to end test for whole package. Aiming to compare performance
     * generating tags on test set.
     */
    @Test
    public void testClassifyMessagesAndRetrieveByTag()
    {
        fail("incomplete, not testing the performance of the test yet");
        Properties props = Utilities.getProperties("storeProperties");
        String folderName = props.getProperty("folder");
        try
        {
            MessageStore msgStore = new MessageStore(props);
            msgStore.setFolder(folderName);
            String trainFilePath = "test/tm470prototype/resources/" + 
                                    "trainingFirst4Normalised.csv";
            
            String testFilePath = "test/tm470prototype/resources/" + 
                                  "test4OfSecond100Normalised.csv";
            String tagSetForTestSetFilePath = "testSetExpectedTagsFilePath";
            Set<ClassifiedMessage> trainingSet = ClassificationUtils.getTrainingSet(trainFilePath,
                    folderName);
            Set<ClassifiedMessage> testSet = ClassificationUtils.getTestSet(testFilePath,
                                                        folderName);
            Set<String> expectedTags = null;
            try
            {
                expectedTags = ClassificationUtils.
                        getExpectedTagSetForTestSet(testFilePath);
            }
            catch (IOException io)
            {
                fail(io.getMessage());
            }
            Tagger bayesTagger = new BayesianTagger();
//            bayesTagger.train(trainingSet);
//            Classifier bayesClassifier = new Classifier(bayesTagger);
//            assertTrue("Expected tags found",
//                    bayesClassifier.test(testSet, expectedTags));
        }
        catch(MessagingException mex)
        {
            fail("Problem connecting to message store" + 
                    mex);
        }
    }
    
}
