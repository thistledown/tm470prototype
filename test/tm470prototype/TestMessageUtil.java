/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Utility class for the test classes, to generate sample messages for testing
 * methods are static as only called to get test messages
 * @author davidpretty
 */
public class TestMessageUtil
{

    /**
     * Returns a MimeMessage with an RFC attachment. Content supplied in parameters.
     * buy adds my details to from and to so this should be amended.
     * @param session the current Javamail session
     * @param plainText The outer message's text
     * @param htmlText the attachment message's text
     * @return a MimeMessage with an RFC attachment
     * @throws MessagingException
     */
    static Message makeMsgWithRFC822Attachment(Session session, String plainText, String htmlText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        Message innerMsg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        bodyPart1.setText(plainText);
        bodyPart2.setContent(htmlText, "text/html; charset=utf-8");
        Multipart innerPart = new MimeMultipart("alternative");
        innerPart.addBodyPart(bodyPart1);
        innerPart.addBodyPart(bodyPart2);
        innerMsg.setContent(innerPart);
        innerMsg.saveChanges();
        msg.setContent(innerMsg, "message/rfc822");
        msg.saveChanges();
        return msg;
    }

    
    /**
     * Returns a plain text message. Content supplied in parameter, though
     * from & to are set to my details, should be changed.
     * 
     * @param session the current Javamail session
     * @param plaintext the message text
     * @return a plaintext message
     * @throws MessagingException
     * 
     */
    static Message makePlainTextMsg(Session session, String plainText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSubject("Test");
        msg.setSentDate(new Date());
        msg.setText(plainText);
        msg.saveChanges();
        return msg;
    }

    /**
     * Returns a  multipart MimeMesssage that contains another multipart
     * Mimemessage as an attachment. Content supplied as parameters
     *. From, To & Subject set for outer message. From & To use my details
     * so should be changed
     * @param session the current Javamail session
     * @param outerText the text of the outer message
     * @param innerText the text of the attached message
     * @return a new multipart MimeMessage
     * @throws MessagingException
     */
    static Message makeNestedMultipart(Session session, String outerText, String innerText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart outerBodyPart = new MimeBodyPart();
        MimeBodyPart innerBodyPart = new MimeBodyPart();
        outerBodyPart.setText(outerText);
        innerBodyPart.setText(innerText);
        Multipart outerPart = new MimeMultipart("mixed");
        MimeBodyPart innerWrap = new MimeBodyPart();
        Multipart innerPart = new MimeMultipart("alternative");
        innerPart.addBodyPart(innerBodyPart);
        innerWrap.setContent(innerPart);
        outerPart.addBodyPart(innerWrap);
        outerPart.addBodyPart(outerBodyPart);
        msg.setContent(outerPart);
        msg.saveChanges();
        return msg;
    }
    
    
    
    
    /**
     * Returns a multipart message that has plain text content and 
     * also contains a html message as an attachment
     * @param session the current Javamail session
     * @param outerPlainText the text of the outer message
     * @param innerHtmlText the text of the inner message
     * @return a new multipart MimeMessage
     * @throws MessagingException 
     */
    static Message makeNestedMultipartWithPlainTextAndHtml(
            Session session, String outerPlainText, String innerHtmlText)
            throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart outerBodyPart = new MimeBodyPart();
        MimeBodyPart innerBodyPart = new MimeBodyPart();
        outerBodyPart.setText(outerPlainText);
        innerBodyPart.setContent(innerHtmlText, "text/html; charset=utf-8");
        Multipart outerPart = new MimeMultipart("mixed");
        MimeBodyPart innerWrap = new MimeBodyPart();
        Multipart innerPart = new MimeMultipart("alternative");
        innerPart.addBodyPart(innerBodyPart);
        innerWrap.setContent(innerPart);
        outerPart.addBodyPart(innerWrap);
        outerPart.addBodyPart(outerBodyPart);
        msg.setContent(outerPart);
        msg.saveChanges();
        return msg;
    }
    
    
    /**
     * Returns a 2 part alternative message with plain text and html
     * alternative message parts. uses my detials in the from and to
     * which should be amended.
     * @param session the current Javamail session
     * @param plainText the text for the plaintext part
     * @param htmlText the text for the html part
     * @return a new multpart MimeMessage
     * @throws MessagingException 
     */
    static Message make2PartAlternative(Session session, String plainText,
            String htmlText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        bodyPart1.setText(plainText);
        bodyPart2.setContent(htmlText, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart("alternative");
        multipart.addBodyPart(bodyPart1);
        multipart.addBodyPart(bodyPart2);
        msg.setContent(multipart);
        msg.saveChanges();
        return msg;
    }
    

    /**
     * Returns a  multipart alternative message, with  plaintext, html
     * & richtext parts. Uses my email addresses in from and to which 
     * I should amend. 
     * @param session the current Javamail session
     * @pararm plainText the text for the plaintText part
     * @param htmlText the text for the html part
     * @param richText the text for the richtext part
     * @return a new multipart MimeMessage
     * @throws MessagingException
     */
    static Message make3PartAlternative(Session session, String plainText,
            String htmlText, String richText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        MimeBodyPart bodyPart3 = new MimeBodyPart();
        bodyPart1.setText(plainText);
        bodyPart2.setContent(htmlText, "text/html; charset=utf-8");
        bodyPart3.setContent(richText, "text/rtf");
        Multipart multipart = new MimeMultipart("alternative");
        multipart.addBodyPart(bodyPart1);
        multipart.addBodyPart(bodyPart2);
        multipart.addBodyPart(bodyPart3);
        msg.setContent(multipart);
        msg.saveChanges();
        return msg;
    }

    /**
     * Returns a  multipart/mixed message, with a plain text and html part
     * @param session the current Javamail session
     * @param plainText the text for the plainText part
     * @param htmlText the text for the html part
     * @return a new multipart/mixed MimeMessage
     * @throws MessagingException
     */
    static Message makeSimple2PartMsg(Session session, String plainText,
            String htmlText) throws MessagingException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        bodyPart1.setText(plainText);
        bodyPart2.setContent(htmlText, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart("mixed");
        multipart.addBodyPart(bodyPart1);
        multipart.addBodyPart(bodyPart2);
        msg.setContent(multipart);
        msg.saveChanges();
        return msg;
    }
    
    
    
    
    /**
     * Returns a 3 part multipart mixed message. One of the message parts
     * Is an attached file. Used for testing code that ensures only the message
     * text is extracted, ignoring the file contents.
     * Uses my details in the from and to, which should be amended
     * @param session the current Javamail session
     * @param firstPart message text
     * @param secondPartFilePath the filepath of the attachment
     * @param thirdPart more message text
     * @return a new MimeMessage containing the 2 string parts and the attached file
     * @throws MessagingException
     * @throws IOException 
     */
    static Message make3PartMultipartMixed(Session session, String firstPart,
            String secondPartFilePath, String thirdPart) throws MessagingException, IOException
    {
        
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        MimeBodyPart bodyPart3 = new MimeBodyPart();
        bodyPart1.setText(firstPart);
        bodyPart2.attachFile(secondPartFilePath);
        bodyPart3.setText(thirdPart);
        Multipart multipart = new MimeMultipart("mixed");
        multipart.addBodyPart(bodyPart1);
        multipart.addBodyPart(bodyPart2);
        multipart.addBodyPart(bodyPart3);
        msg.setContent(multipart);
        msg.saveChanges();
        return msg;
    }
    
    
    
    /**
     * Returns a MimeMessage with one html part
     * @param session the current Javamail session
     * @param htmlText the message text
     * @return the new MimeMessage
     * @throws MessagingException
     * @throws IOException 
     */
    static Message makeSimpleHtmlMessage(Session session, String htmlText) 
            throws MessagingException, IOException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        msg.setContent(htmlText, "text/html; charset=utf-8");
        msg.saveChanges();
        return msg;
    }
    
    
    
    
    /**
     * Returns new MimeMessage containing the supplied plaintext and with 
     * the file attached given the filepath
     * @param session the current Javamail Session
     * @param plainText the message text
     * @param filePath the filepath of the attachment
     * @return
     * @throws MessagingException
     * @throws IOException 
     */
    static Message makePlainTextMsgWithOneAttachment(Session session, 
            String plainText, String filePath) throws MessagingException, IOException
    {
        Message msg = new MimeMessage(session);
        msg.setSubject("Test Subject");
        msg.setFrom(new InternetAddress("davidp@sohoha.org.uk"));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress("davidpretty2001@yahoo.co.uk"));
        msg.setSentDate(new Date());
        MimeBodyPart bodyPart1 = new MimeBodyPart();
        MimeBodyPart bodyPart2 = new MimeBodyPart();
        bodyPart1.setText(plainText);
        bodyPart2.attachFile(filePath);
        Multipart multipart = new MimeMultipart("mixed");
        multipart.addBodyPart(bodyPart1);
        multipart.addBodyPart(bodyPart2);
        msg.setContent(multipart);
        msg.saveChanges();
        return msg;
    }
}
