/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class for the Classifier
 * @author davidpretty
 */
public class ClassifierTest
{
    private javax.mail.Store store;
    private String host, user, password;
    private Properties props;
    private Session session;
    
    
    /**
     * Not used except by Test Framework
     */
    public ClassifierTest()
    {
    }
    
    /**
     * Not used
     */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    /**
     * Sets up the store for testing purposes. Again configuration hard-coded in
     *  NEEDS AMENDMENT
     */
    @Before
    public void setUp()
    {
        store = null;
        
        host = "outlook.office365.com";
        user = "david2@sohoha.org.uk";
        password = "Password!";
        props = new Properties();
        props.setProperty("mail.imap.ssl.enable","true");
        props.setProperty("mail.imap.port", "993");
        session = Session.getInstance(props);
        try
        {
             store = session.getStore("imap"); // this might need to be imaps instead
        }
        catch(NoSuchProviderException ex)
        {
            ex.printStackTrace();
        }
    }
    
    
    /**
     * Closes the store. Is this necessary? Not going consistently.
     */
    @After
    public void tearDown()
    {
        try
        {
            store.close();
        }
        catch(MessagingException mex)
        {
            System.out.println("Problem closing store");
        }
        
    }
    
    
    
    /**
     * Tests retrieving text from a simple text-only email with
     * no attachments
     */
    @Test
    public void testGetMessageTextPlainTextMessage()
    {
         String plainText = "plain text";
         Message msg = null;
         try
         {
             msg = TestMessageUtil.makePlainTextMsg(session, plainText);
         }
         catch(MessagingException mex)
         {
             fail("testGetMessageTextPlainText threw exception");
         }
         Classifier classifier = new Classifier();
         
         assertEquals("getMessage Text should return test text", 
                 plainText, classifier.getMessageText(msg));
     
    }
    
   /**
    * Test  that passing getMessageText() will throw an Exception
    * if passed a null argument
    */
     
    @Test
    public void testGetMessageTextPassingNullThrowsException()
    {
        try
        {
            Classifier classifier = new Classifier();
            classifier.getMessageText(null);
            fail("getMessageText should throw IllegalArgumentException" +
                    "if passed null as argumen");
        }
        catch(IllegalArgumentException iae)
        {
            //do nothing?
        }
    }
    
    
    /**
     * Test that all the message text will be retrieved from a 2 part message
     */
    @Test
    public void testGetMessageText2Part()
    {
        String plainText = "plain Text";
        String htmlText = "<html>Some html</html>";
        String cleanedHtmlText = "Some html";
        Message msg = null;
        String msgTxt = "";
        try
        {
            msg = TestMessageUtil.makeSimple2PartMsg(session,
                    plainText, htmlText);
            Classifier classifier = new Classifier();
            msgTxt = classifier.getMessageText(msg);
        }
        catch(MessagingException mex)
        {
            fail("getMessageText threw exception " + mex);
        }     
        assertTrue("getMessage Text should return 2 test strings" + 
                  "concatenated in any order", msgTxt.contains(plainText) &&
                  msgTxt.contains(cleanedHtmlText)); 
    }
    
    
    
    /**
     * Test that the when getMessageText() is passed a 2 Part Alternative message
     * (which is typically the same text in plaintext and html versions) only a single 
     * plaintext version of the text is returned.
     */
    @Test
    public void testGetMessageText2PartAlternative()
    {
        String plainText = "Message in plain text";
        String htmlText = "<HTML>Message in plain text</HTML>";
        String msgText = "";
        try
        {
            Message msg = TestMessageUtil.make2PartAlternative(session,
                    plainText, htmlText);
            Classifier classifier = new Classifier();
            msgText = classifier.getMessageText(msg);
        }
        catch(MessagingException mex)
        {
            fail("getMessageText threw exception " + mex);
        }
        assertTrue("getMessageText should return plainText only",
                msgText.equals(plainText));
    }  
    
    
    /**
     * Tests that when getMessageText() is passed a 3 part Alternative,
     * only a single plaintext version of the text is returned.
     */
    @Test
    public void testGetMessageText3PartAlternative()
    {
        String plainText = "Some Message Text";
        String hmtlText = "<html>Some Message Text</html>";
        String enrichedText = "<indent>Some Message Text</indent>";
        try
        {
            Message msg = TestMessageUtil.make3PartAlternative(session,
                    plainText, hmtlText, enrichedText);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("getMessageText should return plainText only",
                msgText.equals(plainText));
        
        }
        catch(MessagingException mex)
        {
            fail("getMessageText threw exception " + mex);
        }
    }
    
    
    
    /**
     * Tests that getMessageText() will return all message text when parts
     * are nested in each other.
     */
    @Test
    public void testMessageTextFromNestedParts()
    {
        String innerPart = "This is the first part";
        String outerPart = "this is the second part";
        try
        {
            Message msg = TestMessageUtil.makeNestedMultipart(session,
                    innerPart, outerPart);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("getMessageText sbould reurn string " +
                    "containing inner part and outer part ",
                    msgText.contains(innerPart ) &&
                    msgText.contains(outerPart));
            
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
        
    }
    
    /**
     * Tests that when getMessageText() is passed a message with a 
     * RFC822Attachment all message text will be returned. 
     * At the moment does not do this, 
     * as is only retrieving text from the attachment. AMEND
     */
    @Test
        public void testGetTextMessageWithRFC822Attachment()
    {
        String plainText = "Some plain Text";
        String htmlText = "<html>Some html text</html>";
        String cleanedHtmlText = "Some html text";
        try
        {
            Message msg = TestMessageUtil.makeMsgWithRFC822Attachment(session,
                    plainText, htmlText);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            // enclosed message is a multipart/alternative so only plain
            // text should be returned.
            assertTrue("getMessageText() should return plain text only",
                    msgText.equals(plainText));
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
    }
    
    /**
     * Tests that when getMessageText() is passed a message containing html 
     * text if will return a correct plaintext version of that text.
     */
    @Test
    public void testGetMessageTextFromHtml()
    {
        String hmtlText = "<html><body>" +
                "<p>Some text</p>" +
                "<p>and some more text</p>" +
                "</body></html>";
        String cleanedHtmlText = "Some text and some more text";
        // slipping in an undocumented test to see if removing the html tags
        // is running words together
        try
        {
            Message msg = TestMessageUtil.makeSimpleHtmlMessage(session, hmtlText);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("getMessageText returns cleaned html text",
                    msgText.equals(cleanedHtmlText));
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
        catch(IOException io)
        {
            fail(io.toString());
        }
    }
    
    
    
    // No examples found in sample mails so not doing this.
//    @Test
//    public void testGetMessageTextFromEnriched()
//    {
//        fail("prototype");
//    }
    
    
    // No examples found in sample mails so not doing this.
//    @Test
//    public void testGetMessageTextFromRichText()
//    {
//        fail("prototype");
//    }
    
    /**
     * Tests that getMessageText will return the message text when passed a
     * Multipart/mixed message which is typically a message with images, sound
     * clips etc as inline elements not attachments. Test that <strong>all</strong>
     * the message text and <strong>only</only> the message text is returned.     
     */
    @Test
    public void testGetGetMessageTextFromMulitipartMixed()
    {
        String firstPart = "This is the first part";
        String thirdPart = " and this is the rest of the " +
                            "message text";
        String secondPartPath = "/Users/davidpretty/Documents/Open University/" +
                                "TM470 Project/Software/TM470Prototype/test/" +
                                "tm470prototype/resources/MenAtWork.jpg";
        
        try
        {
            Message msg = TestMessageUtil.make3PartMultipartMixed(session,
                    firstPart, secondPartPath, thirdPart);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("getMessageText should return string containing firstPart " +
                    "and thirdPart", msgText.contains(firstPart) && 
                                     msgText.contains(thirdPart));
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
        catch(IOException io)
        {
            fail(io.toString());
        }   
    }
    
    
    /** 
     * Test that if getMessageText() is passed an message with an attached n
     * non-text file, that it will not return the attachment filepath, only
     * the message text
     */
    @Test
    public void testGetMessageTextNotGetAnthingElse()
    {
        String plainText = "Some plain text";
        String filePath = "/Users/davidpretty/Documents/" +
                "Open University/TM470 Project/Software/" +
                "TM470Prototype/test/tm470prototype/resources/MenAtWork.jpg";
        try
        {
            Message msg = TestMessageUtil.makePlainTextMsgWithOneAttachment(
                    session, plainText, filePath);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("Only message text returned",
                    msgText.equals(plainText));
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
        catch(IOException io)
        {
            fail(io.toString());
        }
    }
    
    
    
    /**
     * Tests that getMessageText() will return all the message text and only
     * the message text from a Multipart/related message.
     */
    @Test
    public void getMessageTextFromMultipartRelated()
    {
        String firstPart = "The first part";
        String thirdPart = "the third part";
        String secondPartFilePath = "/Users/davidpretty/Documents/" +
                "Open University/TM470 Project/Software/" +
                "TM470Prototype/test/tm470prototype/resources/MenAtWork.jpg";
        try
        {
            Message msg = TestMessageUtil.make3PartMultipartMixed(
                    session, firstPart, secondPartFilePath, thirdPart);
            Classifier classifier = new Classifier();
            String msgText = classifier.getMessageText(msg);
            assertTrue("getMessageText() should return text including" +
                    "first and third parts", 
                    msgText.contains(firstPart) &&
                     msgText.contains(thirdPart));
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
        catch(IOException ex)
        {
            fail(ex.toString());
        }
    }
  
    
  /**
   * Tests that the classify method if passed a Message object
   * will return a ClassifiedMessage Object with the same messageText.
   * The Classifier uses a mock Tagger
   * object as we are not testing the performance of the tagger, only the 
   * supporting framework.
   */
  @Test
  public void testClassifyMessageGetsCorrectText()
  {
      String msgText = "Some text mentioning Sandringham Flats";
        try
        {
            Set<String> expectedTags = new HashSet();
            Message testMessage = TestMessageUtil.makePlainTextMsg(session, msgText);
            Tagger mockTagger = new MockTagger();
            Classifier mockClassifier = new Classifier(mockTagger);
            ClassifiedMessage classifiedMsg = mockClassifier.classify(testMessage);
            assertEquals("ClassifiedMessage has  text as that" +
                         "given to argument",
                         msgText, classifiedMsg.getMessageText());
        }
        catch (MessagingException mex)
        {
            fail(mex.getMessage());
        }
  }
  
  /**
   * Tests that the classify method if passed a Message object
   * will return a ClassifiedMessage Object  with the correct set of tags.
   * The Classifier uses a mock Tagger object that will always 
   * return the correct tags
   * as we are not testing the performance of the tagger, only the 
   * supporting framework.
   */
  @Test
  public void testClassifyMessageGetsCorrectTags()
  {
      String msgText = "SFX ASX SJX";
      String[] msgTags = {"SFX", "ASX", "SJX"};
      Set<String> expectedTags = new HashSet(Arrays.asList(msgTags));
        try
        {
            Message testMsg = TestMessageUtil.makePlainTextMsg(session, msgText);
            Tagger mockTagger = new MockTagger();
            Classifier mockClassifier = new Classifier(mockTagger);
            ClassifiedMessage classifiedMsg = mockClassifier.classify(testMsg);
            assertEquals("ClassifiedMessage.getTags() returns expected tags",
                            expectedTags, classifiedMsg.getTags());   
        }
        catch (MessagingException mex)
        {
            fail(mex.getMessage());
        }
  }
   
}
