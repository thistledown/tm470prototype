/*
 * To change this license header, choose License Headers tagsIn Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template tagsIn the editor.
 */

package tm470prototype;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test class for ClassifictionUtils-ClassificaitonUtils is utility class
 * that supports Classifier.
 * @author davidpretty
 */
public class ClassificationUtilsTest
{
    private MessageStore msgStore;
    private String testFolderName;
    
    
    /**
     * Not used except by test framework
    */
    public ClassificationUtilsTest()
    {
    }
    
    /**
     * Not used
     */
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    /**
     * Not used
     * 
     */
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    
    /**
     * sets up email account details, storing configuration in code which
     * is far from ideal.
     * @throws MessagingException 
     */
    @Before
    public void setUp() throws MessagingException
    {
        String testFolderName = "2016 emails";
        Properties storeProps = Utilities.getProperties("storeProperties");
        msgStore = new MessageStore(storeProps);
        msgStore.setFolder(testFolderName);
    }
    
    /**
     * Don't even know if you need to close the message store-need to check
     * @throws MessagingException 
     */
    @After
    public void tearDown() throws MessagingException
    {
        msgStore.close();
    }


    /**
     * Test of getTrainingSet method, of class ClassificationUtils.
     * Tests to see if the expected set of messages is returned.
     * Checks by comparing message ids as the implementation of equals() tagsIn
     n* Message is directly inherited from object so only compares hashcodes
     *
     */
    @Test
    public void testGetTrainingSet()
    {
        String testFilePath = "test/tm470prototype/resources/" + 
                                "trainingFirst4.csv";
        String testFolder = "2016 emails";
        String id1 = "<DB5PR03MB0997DD2AB78E2E4BBD17A6A4E6F20@DB5PR03MB0997."+
                "eurprd03.prod.outlook.com>";
        String id2 = "<HE1PR03MB1339DED66A3250869F3863AF82F20@HE1PR03MB1339." +
                "eurprd03.prod.outlook.com>";
        String id3 = "<HE1PR03MB1339920BE2F57FE5949A846282F20@HE1PR03MB1339." +
                "eurprd03.prod.outlook.com>";
        String id4 = "<HE1PR03MB133985DC3479661891F650AA82F20@HE1PR03MB1339." +
                "eurprd03.prod.outlook.com>";
        
        String[] tags1 = {"PMPM001D002","Occupancy20150","Person13352",
            "helen@sohoha.org.uk","CHIG01"};
        String[] tags2 = {"PMPM001D002", "Occupancy20150", "Person13352", 
            "helen@sohoha.org.uk","CHIG01"};
        String[] tags3 = {"NSNS001D007", "Occupancy10602", "Person10657",
            "Kathy_Meade@shelter.org.uk" ,"Lucie.Cocker@forbessolicitors.co.uk",
            "MIDE01"};
        String[] tags4 = {"NSNS001D007", "Occupancy10602","Person10657",
            "MIDE01"};
        
        Map<String, Set<String>> tagsForIDs = new HashMap();
        tagsForIDs.put(id1, new HashSet(Arrays.asList(tags1)));
        tagsForIDs.put(id2, new HashSet(Arrays.asList(tags2)));
        tagsForIDs.put(id3, new HashSet(Arrays.asList(tags3)));
        tagsForIDs.put(id4, new HashSet(Arrays.asList(tags4)));
        
        Set<String> ids = tagsForIDs.keySet();
 
        final int TRAINING_SET_SIZE = 4;
        try
        {            
            Set<ClassifiedMessage> messages = ClassificationUtils.
                    getTrainingSet(testFilePath, testFolder);
            for(ClassifiedMessage message: messages)
            {
                assertTrue("The id of the classified message is one of those " + 
                        "expected to be in the training set",
                        ids.contains(message.getID()));
                assertEquals("The expected tags are returned for the message id",
                        tagsForIDs.get(message.getID()),
                        message.getTags());
            }
            assertEquals("Both expected and actual sets of messages are of same size",
                    TRAINING_SET_SIZE, messages.size());
        }
        catch(MessagingException mex)
        {
            fail(mex.getMessage());
        }
    }

    /**
     * Tests that getTestSet return a set of ClassifiedMessages
     * which contains the correct original messages and the correct tags.
     * LOTS of hard-coded configuration here. Needs a good re-write
     */
    @Test
    public void testGetTestSet()
    {
        String testFilePath = "test/tm470prototype/resources/" + 
                                "test4OfSecond100.csv";
        String testFolder = "2016 emails";
        String id1 = "<568D38A0.9080407@dchurrell.co.uk>";
        String id2 = "<DB5PR03MB0997D5787A4647AD6B8E1133E6F40@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        String id3 = "<DB5PR03MB0997CC6DC41F54DBAFF8642BE6F40@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        String id4 = "<VI1PR03MB14231D1AAF83302D36BFDC608FF40@VI1PR03MB1423." +
                "eurprd03.prod.outlook.com>";
        
        String[] tags1 = {"DCHU01","RCRC001D006"};
        String[] tags2 = {"CCCC001D013","CHIG01","Occupancy10122"};
        String[] tags3 = {"DCHU01","RCRC001D006"};
        String[] tags4 = {"qube","virtualit.biz"};
        
        Map<String, Set<String>> tagsForIDs = new HashMap();
        tagsForIDs.put(id1, new HashSet(Arrays.asList(tags1)));
        tagsForIDs.put(id2, new HashSet(Arrays.asList(tags2)));
        tagsForIDs.put(id3, new HashSet(Arrays.asList(tags3)));
        tagsForIDs.put(id4, new HashSet(Arrays.asList(tags4)));
        
        Set<String> ids = tagsForIDs.keySet();
        
        final int TEST_SET_SIZE = 4;
                
        try
        {
            Set<ClassifiedMessage> messages = ClassificationUtils.
                    getTestSet(testFilePath, testFolder);
            
            for(ClassifiedMessage message: messages)
            {
                assertTrue("The id of the classified message is one of those " + 
                        "expected to be in the training set",
                        ids.contains(message.getID()));
                assertEquals("The expected tags are returned for the message id",
                        tagsForIDs.get(message.getID()),
                        message.getTags());
            }
            
            assertEquals("Both expected and actual sets of messages are of same size",
                    TEST_SET_SIZE, messages.size());
            
            
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
    }

    /**
     * test that the getExpectedTestSet() method returns the correct set
     * of test tags. LOTs of hard-coded configuration in here.
     */
    @Test
    public void testGetExpectedTagSetForTestSet()
    {
        String testSetFilePath = "test/tm470prototype/resources/" +
                "test4OfSecond100Normalised.csv";
        String testSetExpectedTagsFilePath = "test/tm470prototype/resources/" +
                "testSetFirst4ExpectedTags.csv";
        Set<String> expectedTags = new HashSet();
        File expectedTagsFile = new File(testSetExpectedTagsFilePath);
        Scanner tagScan;
        try
        {
            tagScan = new Scanner(expectedTagsFile);
            while(tagScan.hasNextLine())
            {
                expectedTags.add(tagScan.nextLine());
            }
        }
        catch (FileNotFoundException ex)
        {
            fail(ex.getMessage());
        }
        Set<String> actualTags;
        try
        {
            actualTags = ClassificationUtils.
                    getExpectedTagSetForTestSet(testSetFilePath);
            assertEquals("Tags returned should match those expected",
                expectedTags, actualTags);
        }
        catch (IOException io)
        {
            fail(io.getMessage());
        }
    }
    
    
    /**
     * Tests that the messages retrieved  when getTestSet()
     * is called have the expected IDs & tags and that the correct number of 
     * messages is returned.
     */
    @Test    
    public void testgetMessageSetGivenListInFile()
    {
        String testFilePath = "test/tm470prototype/resources/" + 
                                "test4OfSecond100.csv";
        String testFolder = "2016 emails";
        String id1 = "<568D38A0.9080407@dchurrell.co.uk>";
        String id2 = "<DB5PR03MB0997D5787A4647AD6B8E1133E6F40@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        String id3 = "<DB5PR03MB0997CC6DC41F54DBAFF8642BE6F40@DB5PR03MB0997." +
                "eurprd03.prod.outlook.com>";
        String id4 = "<VI1PR03MB14231D1AAF83302D36BFDC608FF40@VI1PR03MB1423." +
                "eurprd03.prod.outlook.com>";
        
        String[] tags1 = {"DCHU01","RCRC001D006"};
        String[] tags2 = {"CCCC001D013","CHIG01","Occupancy10122"};
        String[] tags3 = {"DCHU01","RCRC001D006"};
        String[] tags4 = {"qube","virtualit.biz"};
        
        Map<String, Set<String>> tagsForIDs = new HashMap();
        tagsForIDs.put(id1, new HashSet(Arrays.asList(tags1)));
        tagsForIDs.put(id2, new HashSet(Arrays.asList(tags2)));
        tagsForIDs.put(id3, new HashSet(Arrays.asList(tags3)));
        tagsForIDs.put(id4, new HashSet(Arrays.asList(tags4)));
        
        Set<String> ids = tagsForIDs.keySet();
        
        final int TEST_SET_SIZE = 4;
                
        try
        {
            Set<ClassifiedMessage> messages = ClassificationUtils.
                    getTestSet(testFilePath, testFolder);
            
            for(ClassifiedMessage message: messages)
            {
                assertTrue("The id of the classified message is one of those " + 
                        "expected to be in the training set",
                        ids.contains(message.getID()));
                assertEquals("The expected tags are returned for the message id",
                        tagsForIDs.get(message.getID()),
                        message.getTags());
            }
            
            assertEquals("Both expected and actual sets of messages are of same size",
                    TEST_SET_SIZE, messages.size());
            
            
        }
        catch(MessagingException mex)
        {
            fail(mex.toString());
        }
    }
    
    
    /**
     * helper method, given a set of messages returns a corresponding  set of
     * of message IDs
     */
    private static Set<String> getIDsFromMessages(Set<Message> messages)
            throws MessagingException
    {
        Set<String> ids = new HashSet();
        for(Message aMessage: messages)
        {
            MimeMessage aMimeMessage = (MimeMessage) aMessage;
            ids.add(aMimeMessage.getMessageID());
        }
        return ids;
    }
}
