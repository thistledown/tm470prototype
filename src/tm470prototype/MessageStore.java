/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;
import javax.mail.search.MessageIDTerm;
import javax.mail.search.SearchTerm;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 * The message store abstracts away the details of accessing email, 
 * so the other parts of the system are not concerned whether 
 * the emails are stored remotely or locally, or what protocol is used to access the messages
 * @author davidpretty
 */
public class MessageStore
{
    Store store;
    Folder folder;
    
    
    /**
     * returns a message store object with no fields set,
     * Not sure if this is of any use.
     */
    private MessageStore()
    {
        super();
    }
    
    
    /**
     * Creates a new message store object based on the properties passed
     * as an argument
     * @param props 
     */
    public MessageStore(Properties props) throws MessagingException
    {
        String host = props.getProperty("host");
        String user = props.getProperty("user");
        String password = props.getProperty("password");
        switch(props.getProperty("protocol"))
        {
            // only supportig imap at present. 
            case "imap":
                Session session = Session.getInstance(props);
                try
                {
                     store = session.getStore("imap"); 
                     try
                    {
                        store.connect(host, user, password);

                    }
                    catch(MessagingException mex)
                    {
                        String message = "Unable to connect to message store";
                        throw new MessagingException(message);
                    }

                }
                catch(NoSuchProviderException ex)
                {
                    ex.printStackTrace();
                }

                break;
            
            default:
                throw new IllegalArgumentException("Protocol " +
                        props.getProperty("protocol") +
                        "not supported");
        }
        
        
        
        if(props.getProperty("protocol").equals("imap"))
        {
            
        }
    }
    
    
    
    /**
     * Returns true if the store has new messages since the last time
     * the method was called. Issue of state here, may need to refer to time
     * or to take an argument of the last messageID checked.
     */
    public boolean hasNewMessages()
    {
        return false;
    }
    
    /**
     * Returns a message to classify. Same state issues as hasNewMessages(()
    so MessageStore instance either needs to record last message checker or
    this method take an argument of last message checked.
     * 
     */
    public Message getMessage()
    {
        return null;    
    }
    
    
    public Map<String, Message> getMessages() throws MessagingException
    {
        Map<String, Message> messages = new HashMap<String, Message>();
        try
        {
            for(Message msg: folder.getMessages())
            {
                MimeMessage mimeMsg = (MimeMessage) msg;
                messages.put(mimeMsg.getMessageID(), msg);
            }
        }
        catch(MessagingException mex)
        {
            throw new MessagingException("Problem accessing Message Store: " +
                    mex);
        }
        return messages;
    }
    
    
    
    /**
     * Method to export messages to CSV for training purposes
     * return type void at present as just printing to System.out
     * for moment.
     */
    public void getMessageDetails(Folder folder)
    {
        
    }
    
    /**
     * Sets the folder that the store uses when
     * accessing messages using the various getMessage and getMessages() methods
     * @param folderName 
     * @throws IllegalArgumentException if a folder of the supplied name
     * does not exist in the message store.
     */
    public void setFolder(String folderName) throws IllegalArgumentException
    {
        try
        {
            if(folder != null)
            {
                if(folder.isOpen())
                {
                    final boolean doNotExpunge = false;
                    folder.close(doNotExpunge);
                }   
            }
            folder = store.getFolder(folderName);
            folder.open(Folder.READ_ONLY);
        }
        catch(MessagingException mex)
        {
            throw new IllegalArgumentException("Folder: " + folderName +
                    "not found");
        }
    }
    
    
    
    /**
     *  exports selected details of the emails in the current folder
     * to a csv file.
     */
    public void exportMailsForClassification(String filepath) throws MessagingException, IOException
    {
        Message[] messages = folder.getMessages();
        
        CSVFormat format = CSVFormat.EXCEL.withHeader(
            "Received", "id", "Subject");
        FileWriter writer = new FileWriter(filepath);
        CSVPrinter csvPrint = new CSVPrinter(writer, format);
        
        
        for(int i=0;  i < messages.length; i++)
        {
            String output = "";
            output += messages[i].getReceivedDate() + ", ";
            String receivedDate = messages[i].getReceivedDate().toString();
            MimeMessage mimeMessage = (MimeMessage) messages[i];
            String id = mimeMessage.getMessageID();
            output += mimeMessage.getMessageID();
            String subject = messages[i].getSubject();
            output += messages[i].getSubject();
            System.out.println(output);
            
            String[] record = {receivedDate, id, subject};
            csvPrint.printRecord((Object) record);
            System.out.println("Exported record " + i);
        }
        writer.flush();
        writer.close();
        csvPrint.close();
    }
    
    /**
     * Returns a set of messages corresponding to the supplied message ids
     * @param IDs the ids of the messages to be retrieved
     * @return a set of messages with the supplied ids
     * @throws MessagingException if any of the messages cannot be retrieved
     * or if there are any other issues with accessing the messages
     */
    public Set<Message> getMessagesByIDs(Set<String> IDs) throws MessagingException
    {
        Set<Message> messages = new HashSet();
        for(String id: IDs)
        {
            messages.add(getMessageByID(id));
        }
        return messages;
    }
    
    /**
     * returns a the message with the supplied id, it it exists
     * in the current folder. Returns null if not found
     * @param ID
     * @return
     * @throws MessagingException if more than one Message with the same id is found
     */
    public Message getMessageByID(String ID) throws MessagingException
    {
        SearchTerm searchTerm = new MessageIDTerm(ID);
        Message[] messages = folder.search(searchTerm);
        if(messages.length == 0)
        {
            throw new MessagingException("No message in folder: " +
                    folder.getName() + 
                    " with  message ID: " + ID);
        }
        else if(messages.length > 1)
        {
            throw new MessagingException("message Id should be unique");
        }
        else
        {
            return messages[0];
        } 
    }
    
    
    
    /**
     * utility method
     * @param args commaond line arguments, not used at present.
     */
        
    public static void main(String[] args)
    {
        
        Properties props = Utilities.getProperties("storeProperties");
        try
        {
            MessageStore msgStore = new MessageStore(props);
            msgStore.setFolder("2016 emails"); 
           
        }
        catch(MessagingException mex)
        {
            System.out.println("Problem connecting to message store" + 
                    mex);
        }
    }
    
    /**
     * closes the message store once no longer needed
     * @throws MessagingException if there are any problems closing the store
     */
    public void close() throws MessagingException
    {
        if(folder != null && folder.isOpen())
        {
            final boolean doNotExpunge  = false;
            folder.close(doNotExpunge);
        }
        if(store != null)   
        {
            store.close();
        }   
    }
    
}
