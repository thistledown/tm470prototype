/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The classifier deals with the details of extracting relevant features 
 * (message content, To, From, sent date etc). 
 * The classifiers could include Bayesian classifiers, 
 * named Entity Recognisers etc. 
 * Each classifier creates zero or more annotations. 
 * The classifier returns a new object classifiedMessage1 
 * wrapping message1 and its annotations.
 * The annotations will be stored as a set of strings.

 * @author davidpretty
 */
public class Classifier
{
    private Tagger tagger;
    
    /**
     * Returns a new classifier object with the tagger not set
     * The classifier returned is only really useful for invoking 
     * the getMessageText() method to extract a message's text.
     */
    public Classifier()
    {
        super();
        this.tagger = null; //not necessary but reinforces that the Classifier
            // returned by the no-args constructor has no tagger.
    }

    /**
     * Returns a new classifier object with the classifiers tagger set to the 
     * supplied argument.
     * @param tagger the tagger the classifier will use to add tags
     * to any messages passed to classify()
     */
    public Classifier(Tagger tagger)
    {
        super();
        this.tagger = tagger;
    }
    
    
    /**
     * Returns a ClassifiedMessage object which is a wrapper around
     * the argument, with a collection of Strings making up the classifications
     * Throws a MessagingException if the constructor cannot access 
     * the message with the given id
     * @param message the message to be classified
     * @return new Classified Message containing the original message
     * and tags relevant to the message 
     */
    public ClassifiedMessage classify(Message message) throws MessagingException
    {
        String msgText = getMessageText(message);
        Set<String> tags = tagger.getTags(msgText);
        MimeMessage mimeMessage = (MimeMessage) message;
        String id = mimeMessage.getMessageID();
        return new ClassifiedMessage(message, msgText, id, tags);
    } 
    
    /**
     * Extracts the text from the argument and returns it as a string.
     * Will examine all the parts of the message that might contain text,
     * excepting non-email attachments (so documents, pictures etc). Uses
     * recursion to examine sub-parts of a message part and any sub-parts
     * of that sub-part etc.
     * @param msg the message 
     * @return the text within the message
     * @throws IllegalArgumentException if the argument is null
     */
    public String getMessageText(Message msg) throws IllegalArgumentException
    {
        String text = "";
        
        //throw Illegal Argument Exception if argument is null
        if(msg == null)
        {
            throw new IllegalArgumentException("argument is null");
        }
        
        try
        {
            text = getTextFromPart(msg);
        }
        catch(MessagingException mex)
        {
            mex.printStackTrace();  //Maybe add problem getting message text classification
        }
        catch(IOException io)
        {
            io.printStackTrace(); // maybe add problem accessing message
        }
            
        return text;
    }
    
    // Helper Methods
    
    
    /**
     * Evaluates the performance of Classifier in terms of the tags if
     * finds for a set of messages vs the expected tags. Not implemented yet
     * @param testSet a set of messages to be examined for tagging
     * @param expectedTags the tags expected to be found from 
     * examining the test set
     * @return true for success, false for failure
     */
    public boolean test(Set<Message> testSet, Set<String> expectedTags)
    {
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    
    
    /**
     * Returns the text contained in the argument
     * Assumes the argument is mimetype
     * text/* type or a multipart/*, or a messsage/rfc822
     * behaviour unpredictable otherwise
     * @param part
     * @return the text included in the part and any sub-parts
     * @throws IOException
     * @throws MessagingException 
     */
    private String getTextFromPart(Part part) throws IOException, MessagingException
    {
        StringBuilder text = new StringBuilder();
        
        if(part.isMimeType("multipart/alternative"))
        {
            // get plain text part, ignore otherwise even though could
            // get plain text from html, however multipart/alternative messages
            // always seem to have plain text parts
            // I am also assmuing there is only 1 plain text part (so there are
            // not plain text parts that area alternatives for each other
            // as i am appending all plain text found 
            Multipart mpt = (Multipart) part.getContent();
            for(int i = 0; i < mpt.getCount(); i++)
            {
                Part thisPart = mpt.getBodyPart(i);
                if(thisPart.isMimeType("text/plain"))
                {
                    String content = getTextFromPart(mpt.getBodyPart(i));
                    text.append(content);
                }
            }
            
            
        }
        else if(part.isMimeType("multipart/mixed") ||
           part.isMimeType("multipart/related"))
        {
            //work through parts looking for text
            Multipart mpt = (Multipart) part.getContent();
            for(int i  = 0; i < mpt.getCount(); i++)
            {
                String content = getTextFromPart(mpt.getBodyPart(i));
                text.append(content);
            }
        }      
        else if (part.isMimeType("multipart/*"))
        {
            Multipart mpt = (Multipart) part.getContent();
            for(int i  = 0; i < mpt.getCount(); i++)
            {
                String content = getTextFromPart(mpt.getBodyPart(i));
                text.append(content);
            }
        }
        
        if(part.isMimeType("message/rfc822"))
        {
            //if rfc822 get text from inside attachment
            text.append(getTextFromPart(
                        (Part) part.getContent()));
        }
        
        
        if(part.isMimeType("text/html"))
        {
            // strip tags out of text-prob similar to text/enriched
            text.append(getPlainTextFromHtmlPart(part));
        }
        else if(part.isMimeType("text/enriched"))
        {
            //strip tags out of text
        }
        else if(part.isMimeType("text/plain"))
        {
            // call append plain text
            text.append(getPlainTextFromPart(part));
        } 
        else if(part.isMimeType("text/*"))
        {
            text.append(getPlainTextFromPart(part));
        }    
        
        return text.toString();    
    }
    
    
    /**
     * Returns the text contained in the argument's content as a string
     * Assumes is passed plain text part, behaviour undefined otherwise.
     * @param part the part 
     * @return the text contained in the part's content
     * @throws IOException if the part cannot be accessed
     * @throws MessagingException not sure why
     */
    private String getPlainTextFromPart(Part part) throws IOException, MessagingException
    {
        return (String) part.getContent();
    }
    
    /**
     * Returns the text contained in the part as plain text, removing any 
     * html tags. Assumes arguments content is passed html text, behaviour
     * undefined otherwise
     * @param part the html part
     * @return the text from the content, minus any html tags
     * @throws IOException
     * @throws MessagingException 
     */
    private String getPlainTextFromHtmlPart(Part part) throws IOException, MessagingException
    {
        String htmlText = (String) part.getContent();
        Document doc = Jsoup.parse(htmlText);
        return doc.text();
    }
        
    
    
}
