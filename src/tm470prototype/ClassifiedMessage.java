/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.mail.Message;

/**
 * Wrapper class for a Message and classifications applied to it.
 * @author davidpretty
 */
public class ClassifiedMessage
{
    private Message message;
    private Set<String> tags;
    private String msgText;
    private String id; 
    
    /**
     * constructor protected as only invoked by Classifier
     * Returns a new ClassifiedMessaage
     */
    protected ClassifiedMessage(Message message, String msgText, String id, 
                                Set<String> tags)
    {
        this.message = message;
        this.msgText = msgText;
        this.tags = tags;
        this.id = id;
    }
    
    /**
     * Returns the text of email message
     * @return the text of the email message
     */
    public String getMessageText()
    {
        return msgText;
    }    
    
    /**
     * Returns a set of strings corresponding to the message tags
     * added by the classifier
     * @return the message tags
     */
    public Set<String> getTags()
    {
        return tags;
    }
    
    /**
     * Returns the messageID of the email message
     * @return the messageID of the email message
     */
    public String getID()
    {
        return id;
    }
    
    
}
