/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.mail.Message;
import tm470prototype.ClassifiedMessage;
import tm470prototype.Tagger;

/**
 * Mock tagger class for testing purposes. Simply takes text from
 * message text and as tags returns a set of strings which are the
 * message text split on whitespace.
 * @author davidpretty
 */
public class MockTagger implements Tagger
{

    /**
     * Only implemented to comply with the interface. 
     * Training would not change the behaviour of instances of this class,
     * so invoking the method will throw an UnsupportedOperationException
     * @param trainingSet 
     */
    @Override
    public void train(Set<ClassifiedMessage> trainingSet)
    {
        throw new UnsupportedOperationException("Mock class cannot be trained");
    }

    
    /**
     * For testing purposes only, just returns a set of string made up of the
     * argument split on whitespace
     * @param text the text to be tagged
     * @return the argument split on whitespace as a Set
     */
    @Override
    public Set<String> getTags(String text)
    {
        String[] tagArray = text.split("\\s+");
        return new HashSet(Arrays.asList(tagArray));
    }
    
    
    
    
}
