/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.Set;
import javax.mail.Message;

/**
 * Interface implemented by Tagger classes.
 * Implementing classes are passed text and return a set of tags for that text
 * to assist with classification
 * @author davidpretty
 */
interface Tagger
{
    /**
     * Trains the tagger given a set of ClassifiedMessages.
     * @param trainingSet 
     */
    public void train(Set<ClassifiedMessage> trainingSet);  
    
    /**
     * Returns a set of tags appropriate for the text supplied as an argument
     * @param text the text supplied as an argument
     * @return a set of Strings representing the tags
     */
    public Set<String> getTags(String text);
}


