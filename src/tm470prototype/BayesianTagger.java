/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.util.HashSet;
import java.util.Set;
import javax.mail.Message;

/**
 * Applies tags using a Naive Bayes Classifier. Has methods for training the
 * classifier and applying tags to messsages once trained.
 * @author davidpretty
 */
class BayesianTagger implements Tagger
{
    Set<String> tags;

    /**
     * Returns a new tagger object will be of little use
     * until trained.
     */
    public BayesianTagger()
    {
        super();
        tags = new HashSet();
    }

    
    /**
     * Trains the inbuilt classifier. The Training set consists of messages 
     * with tags, the classifier learns associations between the tags and the 
     * constituent words in the message
     * @param trainingSet the set of classified messages used to train the tagger
     */
    public void train(Set<ClassifiedMessage> trainingSet)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns a set of tags given the argument. Until the tagger is trained
     * this will just return an empty set 
     * @param text the text to be classified
     * @return the set of tags for the text
     */
    @Override
    public Set<String> getTags(String text)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
