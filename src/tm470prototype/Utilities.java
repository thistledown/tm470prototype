/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import javax.mail.MessagingException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author davidpretty
 * Utility class whole package. At present (4/12/16 ) has
 * methods to set up properties files and return properties objects
 * once passed the name of the relevant property file.
 * 
 */
public class Utilities
{
    /**
     * Utility method just calls setStoreProperties() Used in development, 
     * probably should be removed from finished software
     * @param args 
     */
    public static void main(String[] args)
    {
        //no methods at present
    }
    
    
    
    
    
    /**
     * Saves the values in props to a Properties file with 
     * name: name
     * @param props the Properties object to be saved
     * @param name the name of the Properties file the values are saved to
     */
    public static void storeProperties(Properties props, String name)
    {
        String outFilePath = "resources/" + name;
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(outFilePath);
            props.store(out, null);   
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(out != null)
            {
                try
                {
                    out.close();
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    /**
     *
     * Returns a Properties object once passed a filename-the method
     * reads the contents of the file into the object and returns it.
     * @param name the file name of the properties file
     * @return a Properties object containing the values in the properties file
     */
    
    public static Properties getProperties(String name)
    {
        Properties props = new Properties();
        String inFilePath = "resources/" + name;
        FileInputStream in = null;
        try
        {
            in = new FileInputStream(inFilePath);
            props.load(in);
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(in != null)
            {
                try
                {
                    in.close();
                }
                catch(IOException io)
                {
                    io.printStackTrace();
                }
            }
        }
        return props;
    } 
    
    
    
    public static String getFirstMessageIDFromCSV(String filePath) throws MessagingException
    {
        FileReader in = null;
        String messageID = "";
        CSVParser parser = null;
        try
        {
            in = new FileReader(filePath);
            parser = CSVFormat.EXCEL.withFirstRecordAsHeader().
                    parse(in);
            CSVRecord firstRecord = parser.iterator().next();
            messageID = firstRecord.get("id");
            
        }
        catch (FileNotFoundException ex)
        {
            throw new MessagingException("File not found: " + ex);
        }
        catch (IOException ex)
        {
            throw new MessagingException(
                    "Problem reading csv file specified in " +
                    filePath + ", " + ex);
        }
        catch(Exception ex)
        {
            throw new MessagingException(
                    "Problem accessing message ID value" + ex);
        }  
        finally
        {
            if(parser != null)
            {
                try
                {
                    parser.close();
                }
                catch(IOException io)
                {
                    throw new MessagingException(
                            "Problem closing CSV Parser" + io);
                }                    
            }
            
            if(in != null)
            {
                try
                {
                    in.close();
                }
                catch(IOException io)
                {
                    throw new MessagingException(
                            "Problem closing File Reader" + io);
                }
            }
                
        }
        return messageID;
    }
            
    
}


