/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;

/**
 *
 * @author davidpretty
 */
public class MessageUtil
{
    /**
     * Provides utility methods for getting parts etc out of messages
     * to avoid writing boilerplate code for doing this. All methods static
     * for this reason
     */
    
    
    
    /**
     * Returns an list containing the constituent parts
     * of the argument. Recurses so will return parts of parts etc
     * @param part the argument that is recursed
     * @return a List of the composite parts
     */
    public static List<Part> getPartsRecurse(Part part) throws MessagingException, IOException
    {
        List<Part> parts = new ArrayList<Part>();
        parts.add(part);
        if(part.isMimeType("multipart/*"))
        {
            Multipart mpt = (Multipart) part;
            for(int i = 0; i < mpt.getCount(); i++)
            {
                Part aPart = mpt.getBodyPart(i);
                getPartsRecurse(aPart);
            }
        }
        
        if(part.isMimeType("message/rfc822"))
        {
            Part aPart = (Part) part.getContent();
            getPartsRecurse(aPart);
        }
        
        return parts;
    }
    
    
    /**
     * Returns the base MIME type of the argument rather than the more
     * complete version returned by ContentType(part)
     * @param part
     * @return the base MIME
     * @throws ParseException
     * @throws MessagingException 
     */
    public static String getBaseType(Part part) throws ParseException, MessagingException
    {
        ContentType msgType = new ContentType(part.getContentType());
        return msgType.getBaseType();
    }
    
    
    /**
     * Returns a Set containing the words in the argument
     * a word is defined by breaking the argument into tokens based using whitespace
     * @param text the text
     * @return a Set of Strings representing the words in the text
     */
    public static Set<String> getBagOfWords(String text)
    {
        Set<String> bag = new TreeSet();
        BreakIterator wordIterator = BreakIterator.getWordInstance();
        wordIterator.setText(text);
        int start = wordIterator.first();
        for(int end = wordIterator.next(); end != BreakIterator.DONE;
                start = end, end = wordIterator.next())
        {
            bag.add(text.substring(start, end));
        }  
        return bag;
    }
        
    /**
     * Test & development code
     */
    public static void main(String[] args)
    {
        Properties props = Utilities.getProperties("storeProperties");
        Store store = null;
        
        String host = props.getProperty("host");
        String user = props.getProperty("user");
        String password = props.getProperty("password");
        Session session = Session.getInstance(props);
        try
        {
             store = session.getStore("imap");
        }
        catch(NoSuchProviderException ex)
        {
            ex.printStackTrace();
        }
        try
        {
            if(store != null)
            {
                store.connect(host, user, password);
                
                Folder year2016 = store.getFolder("2016 emails");
                System.out.println("year2016.getMessageCount() = " +
                                    year2016.getMessageCount());
                System.out.println("Now lets try opening year2016");
                try
                {
                    year2016.open(Folder.READ_ONLY);
                }
                catch(MessagingException mex)
                {
                    System.out.println("Error opening folder " + year2016.getName());
                    mex.printStackTrace();
                }
                Classifier classifier = new Classifier();
                Message[] messages = year2016.getMessages();
                System.out.println("Getting text of first message");
                Message firstMsg = messages[0];
                String msgText = classifier.getMessageText(firstMsg);
                System.out.println(msgText);
                System.out.println("Now lets get the bag of words");
                Set<String> firstMsgBag = MessageUtil.getBagOfWords(msgText);
                BreakIterator wordIterator = BreakIterator.getWordInstance();
                wordIterator.setText(msgText);
                System.out.println("Now lets iterate over all the words");
                int start = wordIterator.first();
                for(int end = wordIterator.next(); end != BreakIterator.DONE;
                        start = end, end = wordIterator.next())
                {
                    System.out.println(msgText.substring(start, end));
                }
                
                System.out.println("Now lets see only unqiue instances");
                
                for(String word: firstMsgBag)
                {
                    System.out.println(word);
                }
                
                
                if(year2016.isOpen())
                {
                    year2016.close(false);
                }
            }
            else
            {
                System.out.println("Did not try to connect as store is null");
            }
        }
        catch(MessagingException mex)
        {
            mex.printStackTrace();
        }
        
        
    }
}
