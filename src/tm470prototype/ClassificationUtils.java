/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author davidpretty
 * Class for utilities when classifying message
 */
public class ClassificationUtils
{
    /**
     * Written to acheive whatever task I want to do at the moment
     * which is not very scientific. Maybe reconfigure so takes
     * command line arguments
     * @param args 
     */
    public static void main(String[] args)
    {
        String inFilePath = "/Users/davidpretty/Documents/Open University/"  +
                "TM470 Project/TrainingSet/test4OfSecond100.csv";
        String outFilePath = "/Users/davidpretty/Documents/Open University/" +
                "TM470 Project/TrainingSet/test4OfSecond100Normalised.csv";
       normaliseTaggedFile(inFilePath,outFilePath);
    }
    
    
    /**
     * Takes a input csv file consisting of Message IDs followed by several tags
     * and writes another csv file with a tag per line, "normalising" (need better term)
     * the input
     * @param inFilePath the file path of the input file
     * @param outFilePath the file path of the output file
     */
    public static void normaliseTaggedFile(String inFilePath, String outFilePath)
    {
        Reader in = null;
        Writer out = null;
        CSVPrinter printer = null;
        try
        {
            in = new FileReader(inFilePath);
            out = new FileWriter(outFilePath);
            CSVFormat format = CSVFormat.EXCEL.withHeader(
            "id", "tag");
            printer = new CSVPrinter(out, format);
            
            
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
            for(CSVRecord record: records)
            {
                String messageID = record.get("id");
                for(int i = 3; i < record.size(); i++)
                {
                    String field = record.get(i);
                    if(field.trim().length() > 0)
                    {
                        Object[] line = {messageID, field};
                        printer.printRecord(line);
                    }
                }
            }
        }
        catch(FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(in != null)
            {
                try
                {
                    in.close();
                }
                catch (IOException ex)
                {
                    System.out.println("problem closing Reader for " + inFilePath);
                    ex.printStackTrace();
                }
            }

            try
            {
                if(printer != null)
                {
                    printer.close();
                }
            }
            catch (IOException ex)
            {
                System.out.println("Problen closing CSVPrinter or FileReader");
                ex.printStackTrace();
            }
                
        }
            
    }
    
    /**
     * Returns a set of Classified Messages corresponding to the training set,
     * if passed the filepath and name. 
     * Not strictly necessary as only invokes 
     * getMessageSetGivenListInFile() but added for clarity in the testing code
     * @param filePath the filepath of the training set messages
     * @param folderName the folder name of the training set messages
     * @return a set of Classified Messages
     * @throws MessagingException 
     */
    static Set<ClassifiedMessage> getTrainingSet(String filePath, String folderName) 
            throws MessagingException
    {
        return getMessageSetGivenListInFile(filePath, folderName);
    }

    /**
     * Returns a set of Classified Messages corresponding to the test set,
     * if passed the right file path and folder name. 
     * Not strictly necessary as only invokes 
     * getMessageSetGivenListInFile() but added for clarity in the testing code
     * @param filePath
     * @param folderName
     * @return
     * @throws MessagingException 
     */
    static Set<ClassifiedMessage> getTestSet(String filePath, String folderName) 
            throws MessagingException
    {
        return getMessageSetGivenListInFile(filePath, folderName);
    }

    /**
     * Given the filepath of a csv file containg test messages, will
     * return a set of strings representing all the tags in the csv file.
     * Assumes that tags are in a column with a header "tag"
     * @param tagSetForTestSetFilePath
     * @return 
     */
    static Set<String> getExpectedTagSetForTestSet(String tagSetForTestSetFilePath) 
            throws FileNotFoundException, IOException
    {
       Set<String> tags = new HashSet(); 
       Reader in = null;
       in = new FileReader(tagSetForTestSetFilePath);
       Iterable<CSVRecord> records = CSVFormat.EXCEL.
               withFirstRecordAsHeader().parse(in);
       for(CSVRecord record: records)
       {
           tags.add(record.get("tag"));
       }
       return tags;
    }
        
    /**
     * helper method for getTraiingSet() and getTestSet()
     * returns a set of messages given the filepath of a csv file listing them
     * with a column headed "id" containing the messageIDs. will throw a 
     * MessagingExeption if any of the expected messages are not present in 
     * the current folder.
     */
    private static Set<ClassifiedMessage> getMessageSetGivenListInFile(
            String filePath, String folderName) 
            throws MessagingException
    {
        MessageStore msgStore = null;
        Properties props = Utilities.getProperties("storeProperties");
        Set<ClassifiedMessage> messages = new HashSet();
        Classifier classifier = new Classifier(new MockTagger());
        Reader in = null;
        try
        {
            msgStore = new MessageStore(props);
            msgStore.setFolder(folderName);
            in = new FileReader(filePath);
            CSVParser parser = CSVFormat.EXCEL.withFirstRecordAsHeader().
                    parse(in);
            Map<String, Integer> headerMap = parser.getHeaderMap();
            final int TAGS_START;
            if(headerMap.get("tag") != null) 
            {
                TAGS_START = headerMap.get("tag");
            }
            else
            {
                throw new IllegalArgumentException("file : " + filePath + 
                         " must contain a heading \"tag\"");
            }
            
            
            for(CSVRecord record: parser)
            {
                String messageID = record.get("id");
                Message message = msgStore.getMessageByID(messageID);
                String text = classifier.getMessageText(message);
                Set<String> tags = new HashSet();
                for(int i = TAGS_START; i < record.size(); i++)
                {
                    String tag = record.get(i);
                    if(tag.length() > 0)
                    {
                        tags.add(tag);
                    }
                }
                ClassifiedMessage classifiedMessage = new ClassifiedMessage(
                    message, text, messageID, tags);
                messages.add(classifiedMessage);
            }
        }
        catch (FileNotFoundException ex)
        {
            throw new MessagingException("File not found: " + ex);
        }
        catch (IOException ex)
        {
            throw new MessagingException("unable to acess message: " + ex);
        }
        return messages;
    }
       
    
}
