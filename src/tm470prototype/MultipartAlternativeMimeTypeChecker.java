/*
 * Class to collect statistics as to mimetypes of mutltipart/alternative messages

 */

package tm470prototype;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;



/**
 * Utility class for analysing mail parts to see different types of 
 * Multipart/Alternative, e.g {text/hmtl, text/plain} would be one type and 
 * {text/html,text/plain, text/calendar} would be another
 * @author davidpretty
 */
public class MultipartAlternativeMimeTypeChecker
{
    private Set<Set<String>> altTypes;
    
    /**
     * Creates a new instance with the number of multipart/alternative types
     * set to zero
     */
    public MultipartAlternativeMimeTypeChecker()
    {
        this.altTypes = new HashSet();
    }
    
    
    /**
     * Updates the receiver with the details of the multipart/alternative types
     * of the argument
     * @param part the part to analysed
     * @throws MessagingException if there are problems with the argument's structure
     * @throws IOException if there are problems accessing the argument
     */
    public void updateAltSet(Part part) throws MessagingException, IOException
    {
        if(part.isMimeType("multipart/alternative"))
        {
            Set<String> altSet = new HashSet<>();
            Multipart mpt = (Multipart) part.getContent();
            for(int i = 0; i < mpt.getCount(); i++)
            {
                BodyPart bpt = mpt.getBodyPart(i);
                String baseType = MessageUtil.getBaseType(bpt);
                altSet.add(baseType);
            }
            altTypes.add(altSet); //update if found new combinations of parts
        }
        // recurse if any other kind of Multipart
        else if(part.isMimeType("multipart/*"))
        {
            // iterate through and call method recursively
            Multipart mpt = (Multipart) part.getContent();
            for(int i = 0; i < mpt.getCount(); i++)
            {
                updateAltSet((Part) mpt.getBodyPart(i));
            }
        }
        
        // recurse also if rfc822 for emebedded messages
        if(part.isMimeType("message/rfc822"))  
        {
            updateAltSet((Part) part.getContent());
        }
    }

    /**
     * Prints the details of the statistics collected re multipart/alternative
     * types to standard output
     */
    public void outputAltSet()
    {
        System.out.println("Listing multipart/aternative subtypes");
        int i = 1;
        for(Set<String> membSet: this.altTypes)
        {
            System.out.println("Type : " + i++);
            for(String type: membSet)
            {
                System.out.println(type);
            }       
        }
    }
    
    /**
     * Returns the number of different multipart/alternative MIME types found.
     * @return 
     */
    public int getNoTypes()
    {
        return this.altTypes.size();
    }
    
    /**
     * Returns true if the MIME type given in the argument is listed
     * in the receiver as a component of the multipart/alternative types, 
     * false other wise
     * @param subType a string description of a MIME type
     * @return whether the MIME type is listed in the receiver's 
     * multipart/alternative types.
     */
    public boolean containsSubType(String subType)
    {
        boolean contains = false;
        for(Set<String> type:altTypes)
        {
            contains = type.contains(subType);
            if(contains)
            {
                break;
            }
        }
        return contains;
    }
    
    /**
     * Returns true if the set of MIME types is listed by the receiver
     * @param type a Set of Strings listing the composite MIME types,
     * false otherwise
     * @return whether the receiver lists a multipart/alternative type
     * matching the argument.
     */
    public boolean containsType(Set<String> type)
    {
        return altTypes.contains(type);
    }
    
}

        
