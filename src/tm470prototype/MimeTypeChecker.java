/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tm470prototype;

import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;

/**
 * Utility class for collecting details of the MIME types of messages or 
 * message parts passed to an instance's updateMimeType... methods
 * @author davidpretty
 */
    public class MimeTypeChecker
{
    private SortedMap<String, Integer> mimeTypes;
    
    
    
    
    /**
     * returns a new instance with a count of zero for all MIME types 
     */
    public MimeTypeChecker()
    {
        super();
        mimeTypes = new TreeMap<String, Integer>();
    }
      
    /**
     * updates the receiver's Mime Types with the Mime Type
     * of arguments header-does not recurse through the composite parts 
     * of the argument
     * @param part the mail part hat is analysed
     * @throws javax.mail.MessagingException if there are problems with the arguments structure
     * @throws java.io.IOException if there are problems accessing the argument
     */
    public void updateMimeTypesHeaderOnly(Part part) throws MessagingException, IOException
    {
        ContentType msgType = new ContentType(part.getContentType());
        String msgBaseType = msgType.getBaseType();
        if(mimeTypes.containsKey(msgBaseType))
        {
            int newCount = mimeTypes.get(msgBaseType) + 1;
            mimeTypes.put(msgBaseType, newCount);
        }
        else
        {
            mimeTypes.put(msgBaseType, 1);
        }

    }

    
    
    /**
     * updates the receiver's Mime Types based with the Mime Type of 
       the argument-recurses through any composite parts
     * @param part the mail part that is analysed
     * @throws javax.mail.MessagingException if there are problems with the arguemnt's structure
     * @throws java.io.IOException if there are problems accessing the argument
     */
    public void updateMimeTypesRecurse(Part part) throws MessagingException, IOException
    {
        //to start with just do not recurse, just do this Part
        updateMimeTypesHeaderOnly(part);
        
        //Now add recursion for types containig subparts
        
        if(part.isMimeType("multipart/*"))
        {
            Multipart mpt = (Multipart) part.getContent();
            for(int i = 0; i < mpt.getCount(); i++)
            {
                updateMimeTypesRecurse((Part) mpt.getBodyPart(i));
            }
                
        }
        
        if(part.isMimeType("message/rfc822"))
        {
            updateMimeTypesRecurse((Part) part.getContent());
        }
        
        
    }
    
    /**
     * Returns the number of different mime types in messages and message parts
     * the receiver has been passed
     * 
     * @return the number of different MIME types
     */
    public int getNoDiffMimeTypes()
    {
        return mimeTypes.size();
    }
    
    /**
     * returns the total number of MIME types found 
     * including multiple instances of the same type.
     * e.g if 2 examples of text/plain and 5 examples of text/hmtl
     * found, would return 7
     * @return the number of MIME types
     */
    public int getNoMimeTypes()
    {
        int noEntries = 0;
        for(String mimeType: mimeTypes.keySet())
        {
            noEntries += mimeTypes.get(mimeType);
        }
        return noEntries;
    }
    
    
    /**
     * Prints type and frequencies of MIME types found to System.out
     */
    public void outputMimeTypes()
    {
        System.out.println("No if different Mime Types =  " 
                + this.getNoDiffMimeTypes() );
        System.out.println("Total No of Mime Types found = " +
                            this.getNoMimeTypes());
        for(String mimeType: mimeTypes.keySet())   
        {
            System.out.println("Mime type = " + mimeType + 
                    ", count =  " + mimeTypes.get(mimeType));
        }
    }
   
}
